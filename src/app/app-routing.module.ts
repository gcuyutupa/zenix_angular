import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './omega/dashboard/dashboard.component';
import { OmegaComponent } from './omega/omega.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'omega', component: OmegaComponent, children: [
    {path: 'dashboard', component: DashboardComponent},
    {path: '', redirectTo: 'dashboard', pathMatch: 'full'}
  ]},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
