import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private API_URL = environment.API_URL;
  tokens : string[] = [];
  loginStatus = '';

  loginForm = new FormGroup({
    user: new FormControl('', [Validators.required]),
    passwd: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(20)])
  });

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
  }

  login(){
    if(this.loginForm.get('user')?.value == '' || this.loginForm.get('passwd')?.value == ''){
      this.loginStatus = "Incomplete fields...";
      alert(this.loginStatus);
    }else{
      this.http.post(this.API_URL + 'systemprocess/login', this.loginForm.value).subscribe(res=>{
        this.tokens = Object.values(res);
        //console.log(this.tokens[0]);
        if(this.tokens[0] == 'User or Passw incorrect' || this.tokens[0] == 'User not found'){
          this.loginStatus = this.tokens[0];
          alert(this.loginStatus);
        }else{
          this.loginStatus = "Accesing...";
          sessionStorage.setItem('token', 'Bearer ' + this.tokens[0]);
          this.router.navigate(['/omega']);
          
        }
      });
      return false;
    }
    return false
  }
  
}
