import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-omega',
  templateUrl: './omega.component.html',
  styleUrls: ['./omega.component.css']
})
export class OmegaComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    
  }

  logout(){
    sessionStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

}
